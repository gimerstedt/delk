#!/bin/sh
apk add --no-cache bc
i=0
while true; do
  echo "We're currently cruising along at ${i} kph, hold on to your hat..."
  i=$(echo "$i+1" | bc)
  sleep 1
done
