.PHONY: default
default: run

.PHONY: run
run: prereq elasticsearch wait logstash kibana

.PHONY: prereq
prereq: network clean

.PHONY: wait
wait:
	sleep 4

.PHONY: network
network:
	@echo 'Creating logging networks...'
	@docker network ls | grep -q logging || docker network create logging
#	@docker network ls | grep -q apps || docker network create apps

.PHONY: clean
clean:
	@echo 'Cleaning up old containers...'
	-docker stop elasticsearch logstash kibana
	-docker rm -vf elasticsearch logstash kibana

.PHONY: elasticsearch
elasticsearch:
	@echo 'Starting Elasticsearch...'
	@docker run \
		-d \
		--network logging \
		--name elasticsearch \
		elasticsearch

.PHONY: logstash
logstash:
	@echo 'Starting Logstash...'
	@docker run \
		-d \
		--network logging \
		-p 12201:12201 \
		-p 12201:12201/udp \
		--name logstash \
		-v $(shell pwd)/logstash.conf:/config-dir/logstash.conf \
		logstash \
		logstash -f /config-dir/logstash.conf

.PHONY: kibana
kibana:
	@echo 'Starting Kibana...'
	@docker run \
		-d \
		--network logging \
		-p 80:5601 \
		--name kibana \
		kibana

.PHONY: test
test:
	-docker stop nginx
	-docker rm -vf nginx
	docker run \
		-d \
		-p 1234:80 \
		--name nginx \
		--log-driver gelf \
		--log-opt gelf-address=udp://localhost:12201 \
		--log-opt tag="nginx" \
		-v $(shell pwd)/loop.sh:/loop.sh \
		nginx:alpine
	docker exec -id nginx sh -c "sh /loop.sh"

.PHONY: test2
test2:
	-docker stop asdf
	-docker rm -vf asdf
	docker run \
		-d \
		--name asdf \
		--log-driver gelf \
		--log-opt gelf-address=udp://localhost:12201 \
		--log-opt tag="asdf" \
		-e i=0 \
		-v $(shell pwd)/loop2.sh:/loop.sh \
		alpine \
		sh -c "sh /loop.sh"
